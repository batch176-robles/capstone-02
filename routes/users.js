//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controllers/users');
const auth = require('../auth');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routes

// Registration
route.post('/register', controller.register);


// Login
route.post('/login', controller.login);


// Delete User
route.delete('/delete', auth.verifyUser, auth.verifyAdmin, controller.deleteUser);


// Retrieve all users
route.get('/all', auth.verifyUser, auth.verifyAdmin, controller.getAll);




// Retrieve User
/* route.get('/details', auth.verifyUser, controller.getUser);
 */
/* route.get('/details', auth.verify, (req, res) => {
    controller.getUser(req.user.id).then(result => res.send(result));
}) */

// route.get('/details', auth.verify, controller.getUser);

route.get('/details', auth.verifyUser, (req, res) => {
    controller.getProfile(req.user.id).then(result => res.send(result))
});

// Update User
route.put('/:userId', auth.verifyUser, controller.updateUser);

// Set Admin
route.put('/:userId/admin', auth.verifyUser, auth.verifyAdmin, controller.setAdmin);

// Remove Admin
route.put('/:userId/remove', auth.verifyUser, auth.verifyAdmin, controller.removeAdmin);


// Retrieve all Admin Users
route.get('/all-admin', auth.verifyUser, auth.verifyAdmin, controller.allAdmin);



// Retrieve all Normal Users Only
route.get('/all-usersonly', auth.verifyUser, auth.verifyAdmin, controller.allNorm);

module.exports = route;

