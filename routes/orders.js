//[SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth')
const controller = require('../controllers/orders');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routes

// route.post('/order', auth.verifyUser, controller.getOrder);
// Checkout
route.post('/:userId/checkout', auth.verifyUser, controller.checkout);


//Retrieve User's Order
route.get('/:userId/view', auth.verifyUser, controller.getUserOrder);


// Retrieve ALL Orders
route.get('/all', auth.verifyUser, auth.verifyAdmin, controller.getAll);


// Delete Order
route.delete('/delete', auth.verifyUser, auth.verifyAdmin, controller.deleteOrder)

module.exports = route;