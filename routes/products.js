//[SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth')
const controller = require('../controllers/products');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routes- POST

// Create Product
route.post('/create', auth.verifyUser, auth.verifyAdmin, controller.addProduct);


// Retrieve all ACTIVE
route.get('/active', controller.getAllActive);


// Retrieve a SINGLE product
route.get('/:productId/detail', (req, res) => {
    console.log(req.params.productId);
    controller.getProduct(req.params.productId).then(result => res.send(result));
})


// Updating Product Info
// route.put('/:productId/update', controller.updateProduct);

route.put('/:productId', (req, res) => {
    controller.updateProduct(req.params.productId, req.body).then(result => res.send(result));
})


// Archive Product
// route.put('/:productId/archive', auth.verifyUser, auth.verifyAdmin, controller.archiveProduct);

route.put('/:productId/archive', auth.verifyUser, auth.verifyAdmin, (req, res) => {
    controller.archiveProduct(req.params.productId).then(result => res.send(result));
})



// Activate Product
// route.put('/:productId/activate', auth.verifyUser, auth.verifyAdmin, controller.reactivateProduct);
route.put('/:productId/activate', auth.verifyUser, auth.verifyAdmin, (req, res) => {
    controller.reactivateProduct(req.params.productId).then(result => res.send(result));
})

// Delete Product
route.delete('/delete', auth.verifyUser, auth.verifyAdmin, controller.deleteProduct);


// Retrieve Archived Products
route.get('/inactive', auth.verifyUser, auth.verifyAdmin, controller.getAllInactive);


// Product on Sale
route.put('/sale', auth.verifyUser, auth.verifyAdmin, controller.productSale);

// Return Product to Normal
route.put('/normal', auth.verifyUser, auth.verifyAdmin, controller.productNorm);


// Retrieve ALL Products
/* route.get('all', (req, res) => {
    controller.getAllProducts().then(result => res.send(result));
})
 */
route.get('/all', controller.getAll);




//[SECTION] Expose Route System
module.exports = route;