//[SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth')
const controller = require('../controllers/carts');
const product = require('../models/product');
const user = require('../models/user');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routes

// View all in Cart
route.get('/:userId/view', auth.verifyUser, controller.viewCart);

// Add Cart
route.post('/:userId/add', auth.verifyUser, controller.addToCart);


// Edit Quantity Cart
route.put('/:userId/edit', auth.verifyUser, controller.editCart);


// // Remove to Cart
route.put('/:userId/remove', auth.verifyUser, controller.removeToCart);



module.exports = route;