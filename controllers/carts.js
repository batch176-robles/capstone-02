//[SECTION] Dependencies and Modules
const Cart = require('../models/cart');
const User = require('../models/user');
const Product = require('../models/product')
const dotenv = require('dotenv');
const { search } = require('../routes/orders');

//[SECTION] Environment Variables Setup
dotenv.config();

//[SECTION] Functionalities 

// View all in Cart
module.exports.viewCart = (req, res) => {
    return Cart.find({ User: req.params.userId }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}

// Add to Cart
module.exports.addToCart = (req, res) => {

    return Cart.findOne({ User: req.params.userId }).then(cart => {
        if (cart) {
            Product.findById(req.body.productId).then(product => {
                if (product) {
                    let ordering =
                    {
                        productID: req.body.productId,
                        productName: product.name,
                        productPrice: product.price,
                        quantity: req.body.quantity,
                        subTotal: product.price * req.body.quantity
                    };

                    let newPrice = (product.price * req.body.quantity + cart.totalAmount)

                    cart.Product.push(ordering);
                    cart.totalAmount = newPrice;
                    console.log(cart)

                    return cart.save().then(success => {
                        res.send(success)
                    }).catch(err => res.send(err))

                } else {
                    res.send({ message: `No match found for productId ${req.body.productId}` })
                }
            })
        } else {
            Product.findById(req.body.productId).then(product => {

                if (product) {
                    let newCart = new Cart({
                        User: req.params.userId,
                        Product: [
                            {
                                productID: req.body.productId,
                                productName: product.name,
                                productPrice: product.price,
                                quantity: req.body.quantity,
                                subTotal: product.price * req.body.quantity
                            }],
                        totalAmount: (product.price * req.body.quantity)
                    })
                    return newCart.save().then(result => {
                        res.send(result);
                    }).catch(err => res.send(err))

                } else {
                    res.send({ message: `No match found for productID ${req.body.userId}` })
                }
            })

        }
    }).catch(error => error)
}

module.exports.removeToCart = (req, res) => {


    return Cart.findOne({ User: req.params.userId }).then(cart => {
        if (cart) {

            // // console.log(cart.Product)
            let findCart = cart.Product.find(product => product._id.toString() === req.body._id);

            // console.log({ findCart })

            let searchIndex = cart.Product.findIndex(product => product._id.toString() == req.body._id);

            cart.Product.splice(searchIndex, 1);

            let newTotal = cart.totalAmount - findCart.subTotal;
            cart.totalAmount = newTotal;
            return cart.save().then(success => {
                res.send(true)
            }).catch(err => res.send(err))
        }
    }).catch(error => error)
}


module.exports.editCart = (req, res) => {

    let updatedQuantity = {
        quantity: req.body.quantity
    }

    return Cart.findOne({ User: req.params.userId }).then(cart => {
        if (cart) {


            // let findCart = cart.Product.find(c => c._id == req.body._id);
            // let searchIndex = cart.Product.findIndex((c) => c._id == req.body._id);

            console.log(cart)

            // cart.Product.splice(searchIndex, 1);
            // let newPrice = (cart.totalAmount - findCart.subTotal);
            // cart.totalAmount = newPrice;

            // return cart.save().then(success => {
            //     res.send(success)
            // }).catch(err => res.send(err))



        }
    }).catch(error => error)
}