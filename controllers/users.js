//[SECTION] Dependencies and Modules
const User = require('../models/user');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const auth = require('../auth');
const { db } = require('../models/user');


//[SECTION] Environment Variables Setup
dotenv.config();
const salt = Number(process.env.SALT);

//[SECTION] Functionalities [CREATE]

// User Registration
module.exports.register = (req, res) => {
    if (req.body.email !== '' && req.body.password !== '') {
        return User.findOne({ email: req.body.email }).then(result => {
            if (result) {
                res.send(false)

            } else {
                let newUser = new User({
                    email: req.body.email,
                    password: bcrypt.hashSync(req.body.password, salt)
                });
                return newUser.save().then(result => {
                    res.send(true);
                })
            }
        }).catch(err => res.send(err))
    } else {
        res.send(false)
    }
}

// User Login
module.exports.login = (req, res) => {
    if (req.body.email !== '' && req.body.password !== '') {
        return User.findOne({ email: req.body.email }).then(result => {
            if (result == null) {
                return false;
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
                if (isPasswordCorrect) {
                    res.send({ accessToken: auth.createAccessToken(result.toObject()) })
                } else {
                    res.send({ message: 'Failed to login' })
                }
            }
        }).catch(err => res.send(err))
    } else {
        res.send({ message: 'Please fill up all fields' })
    }
}


// User Delete
module.exports.deleteUser = (req, res) => {
    return User.findById(req.body.userId).then(result => {
        if (result) {
            return User.findByIdAndRemove(req.body.userId).then(result => {
                res.send({ message: `Successfully deleted User ${req.body.userId}` })
            })
        } else {
            res.send({ message: 'No Match found' })
        }
    }).catch(error => error)

}

// Retrieve All Users
module.exports.getAll = (req, res) => {
    return User.find({}).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}


// Retrieve User
module.exports.getProfile = (data) => {
    return User.findById(data).then(result => {
        //change the value of the user's password to an empty string
        result.password = '';
        return result;
    })
}


// Update User
module.exports.updateUser = (req, res) => {
    if (req.body.email !== '' && req.body.password !== '' && req.params.userId !== ':userId') {


        let updatedUser = {
            email: req.body.email,
            password: req.body.password
        }
        return User.findByIdAndUpdate(req.params.userId, updatedUser).then((user, error) => {
            if (error) {
                res.send({ message: 'Failed to update user' })
            } else {
                res.send({ message: 'Successfully updated the user' })
            }
        }).catch(err => res.send(err))
    } else {
        res.send({ message: 'Please fill up all fields' })
    }
}


// Set Admin
module.exports.setAdmin = (req, res) => {
    let updatedField = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(req.params.userId, updatedField).then((user, error) => {
        if (error) {
            res.send({ message: 'Failed to set user as Admin' })
        } else {
            res.send({ message: 'Successfully set user as Admin' })
        }
    }).catch(err => res.send(err))
}



// Remove Admin
module.exports.removeAdmin = (req, res) => {
    let updatedField = {
        isAdmin: false
    };

    return User.findByIdAndUpdate(req.params.userId, updatedField).then((user, error) => {
        if (error) {
            res.send({ message: `Failed to remove User ${req.params.userId} as Admin` })
        } else {
            res.send({ message: `Successfully remove User ${req.params.userId} as Admin` })
        }
    }).catch(err => res.send(err))
}

// Retrieve All Admin
module.exports.allAdmin = (req, res) => {
    return User.find({ isAdmin: true }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}

// Retrieve All Admin
module.exports.allNorm = (req, res) => {
    return User.find({ isAdmin: false }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}