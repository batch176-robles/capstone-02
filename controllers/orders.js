//[SECTION] Dependencies and Modules
const Order = require('../models/order');
const Cart = require('../models/cart');
const Product = require('../models/product')
const dotenv = require('dotenv');

//[SECTION] Environment Variables Setup
dotenv.config();

//[SECTION] Functionalities 

// Create Order
// module.exports.getOrder = (req, res) => {
//     if (req.body.productId !== '' && req.body.userId !== '' && req.body.quantity !== '') {
//         return Product.findById(req.body.productId).then(result => {
//             if (result) {
//                 if (result.isActive !== false) {
//                     let newOrder = new Order({
//                         User: req.body.userId,
//                         Product: [
//                             {
//                                 productID: req.body.productId,
//                                 quantity: req.body.quantity
//                             }],
//                         totalAmount: result.price * req.body.quantity
//                     })
//                     return newOrder.save().then(result => {
//                         res.send(result);
//                     }).catch(err => res.send(err))
//                 } else {
//                     res.send({ message: `Sorry ${result.name} is currently not available.` })
//                 }
//             } else {
//                 res.send({ message: 'Product not found!' })
//             }
//         }).catch(err => res.send(err))
//     } else {
//         res.send({ message: 'Please fill up all fields' })
//     }

// }

module.exports.checkout = (req, res) => {

    return Cart.find({ User: req.params.userId }).then(result => {
        if (result) {
            let newOrder = new Order(
                {
                    User: req.params.userId,
                    Product: result[0].Product,
                    totalAmount: result[0].totalAmount,
                });

            // res.send(ordering)

            return Order.findOne({ User: req.params.userId }).then(() => {
                return newOrder.save().then(result1 => {
                    if (result1) {
                        return Cart.findOneAndDelete({ User: req.params.userId }).then(okay => {
                            if (okay) {
                                res.send(true)
                            } else {
                                res.send(false)
                            }
                        })
                    } else {
                        res.send(false)
                    }

                })


            }).catch(err => res.send(err))
        } else {
            res.send(false)
        }
    }).catch(err => res.send(err))
}


// Retrieve User's Order
module.exports.getUserOrder = (req, res) => {
    return Order.find({ User: req.params.userId }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}



// Retrieve ALL Orders
module.exports.getAll = (req, res) => {
    return Order.find({}).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}


// Delete Order
module.exports.deleteOrder = (req, res) => {

    return Order.findById(req.body.orderId).then(result => {
        if (result) {
            return Order.findByIdAndRemove(req.body.orderId).then(result => {
                res.send({ message: `Successfully deleted Order ${req.body.orderId}` })
            })
        } else {
            res.send({ message: 'No Match found' })
        }
    }).catch(error => error)

}