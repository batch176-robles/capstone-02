//[SECTION] Dependencies and Modules
const Product = require('../models/product');
const dotenv = require('dotenv');

//[SECTION] Functionalities 

// Create Product
module.exports.addProduct = (req, res) => {
    if (req.body.name !== '' && req.body.description !== '' && req.body.price !== '') {
        return Product.findOne({ name: req.body.name }).then(result => {
            if (result) {
                res.send({ message: 'This Product Name is already registered. Please input a new one' })

            } else {
                let newProduct = new Product({
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price
                })

                return newProduct.save().then(result => {
                    res.send(result);
                })
            }
        }).catch(err => res.send(err))
    } else {
        res.send({ message: 'Please fill up all fields' })
    }
}

// Retrieve all ACTIVE
module.exports.getAllActive = (req, res) => {
    return Product.find({ isActive: true }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}

// Retrieve a SINGLE Product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result;
    }).catch(error => error)
}


// Updating Product Info
module.exports.updateProduct = (productId, data) => {
    let updatedProduct = {
        name: data.name,
        description: data.description,
        price: data.price
    }

    return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}

// Archive Product
module.exports.archiveProduct = (productId) => {
    let updateActiveField = {
        isActive: false
    };
    return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}

// Delete Product
module.exports.deleteProduct = (req, res) => {
    return Product.findById(req.body.productId).then(result => {
        if (result) {
            return Product.findByIdAndRemove(req.body.productId).then(result => {
                res.send({ message: `Successfully deleted Product ${req.body.productId}` })
            })
        } else {
            res.send({ message: 'No Match found' })
        }
    }).catch(error => error)

}

// Retrieve all ACTIVE
module.exports.getAllInactive = (req, res) => {
    return Product.find({ isActive: false }).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}

// Reactivate Product
module.exports.reactivateProduct = (productId) => {
    let updateActiveField = {
        isActive: true
    };

    return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}

// Single Product on Sale
module.exports.productSale = (req, res) => {
    return Product.findById(req.body.productId).then(result => {
        if (result.promo[0].isOnSale !== true) {
            let updatePromo = {
                price: result.price - req.body.discount,
                promo: [{
                    isOnSale: true,
                    discount: req.body.discount
                }]
            }

            return Product.findByIdAndUpdate(req.body.productId, updatePromo).then((product, error) => {
                if (error) {
                    res.send({ message: 'Failed to put on sale the product' })
                } else {
                    res.send({ message: `${result.name} is now on sale at ${updatePromo.price}` })
                }
            }).catch(err => res.send(err))
        } else {
            res.send({ message: 'Product already on sale' })
        }
    }).catch(err => res.send(err))
}

// Single Product Return to Normal
module.exports.productNorm = (req, res) => {
    return Product.findById(req.body.productId).then(result => {

        if (result) {
            let updatePromo = {
                price: result.price + result.promo[0].discount,
                promo: [{
                    isOnSale: false,
                    discount: 0
                }]
            }

            return Product.findByIdAndUpdate(req.body.productId, updatePromo).then((product, error) => {
                if (error) {
                    res.send({ message: 'Failed to revert product' })
                } else {
                    res.send({ message: `Removed item sale for ${result.name}.` })
                }
            }).catch(err => res.send(err))
        } else {
            res.send({ message: 'Product not found' })
        }
    }).catch(err => res.send(err))
}


// Retrieve all ALL
module.exports.getAll = (req, res) => {
    return Product.find({}).then(result => {
        res.send(result)
    }).catch(err => res.send(err))
}

/* module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    }).catch(error => error)
}

 */