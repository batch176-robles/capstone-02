// [SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const userRoutes = require('./routes/users');
const prodRoutes = require('./routes/products');
const orderRoutes = require('./routes/orders');
const cartRoutes = require('./routes/carts');



// [SECTION] Environment Setup
dotenv.config();

let account = process.env.CREDENTIALS;
const port = process.env.PORT;

// [SECTION] Server Setup
const app = express();
app.use(express.json());
app.use(cors());

// [SECTION] Database Connection
mongoose.connect(account);
const connectStatus = mongoose.connection;
connectStatus.once('open', () => console.log(`Connected to the WeekendBakerMNL cloud database`));


// [SECTION] Backend Routes
app.use('/users', userRoutes);
app.use('/products', prodRoutes);
app.use('/orders', orderRoutes);
app.use('/carts', cartRoutes);





// [SECTION] Server Gateway Response
app.get('/', (req, res) => {
    res.send(`Welcome to Weekend Baker MNL`)
});
app.listen(port, () => {
    console.log(`Initializing. Server running at port ${port}`);
});