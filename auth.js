const jwt = require('jsonwebtoken');
const secret = 'WeekendBakerMNL';

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
}

module.exports.verifyUser = (req, res, next) => {
    let token = req.headers.authorization;
    if (typeof token === 'undefined') {
        return res.send({ auth: 'Failed. No token provided' });
    } else {
        token = token.slice(7, token.length)

        jwt.verify(token, secret, function (err, decodedToken) {
            if (err) {
                return res.send({
                    auth: "Verification failed",
                    message: err.message
                })
            } else {
                req.user = decodedToken
                next();
            }
        })
    }
}

module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin == true) {
        next();
    } else {
        return res.send({
            message: 'Action Forbidden. User is not an admin'
        })
    }
}