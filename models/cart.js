// [SECTION] Modules and Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema/Blueprint
const cartSchema = new mongoose.Schema({

    User:
    {
        type: String,
        required: [true, 'User ID is Required']
    },
    Product: [
        {
            productID: {
                type: String,
                required: [true, 'Product ID is Required']
            },
            productName: {
                type: String,
                required: [true, 'Product name is Required']
            },
            productPrice: {
                type: Number,
                default: 0
            },
            quantity: {
                type: Number,
                default: 1,
                required: [true, 'Quantity is Required'],
            },
            subTotal: {
                type: Number,
                default: 0
            }
        }],

    totalAmount: {
        type: Number,
        default: 0
    }
});

// [SECTION] Model
module.exports = mongoose.model('Cart', cartSchema);