// [SECTION] Modules and Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema/Blueprint
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Product Name is Required']
    },
    description: {
        type: String,
        required: [true, 'Description is Required']
    },
    price: {
        type: Number,
        required: [true, 'Product Price is Required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    promo: [
        {
            isOnSale:
            {
                type: Boolean,
                default: false
            },
            discount: {
                type: Number,
                default: 0
            }
        }],
    createdOn: {
        type: Date,
        default: new Date()
    }
});

// [SECTION] Model
module.exports = mongoose.model('Product', productSchema);